/**
 * Created by IvanIsrael on 21/09/2015.
 */
'use strict';
var app = angular.module('Dashboard', [
  'ngAnimate',
  'ui.bootstrap',
  'ui.router',
  'ngResource',
  'ngSanitize',
  'ngCookies',
  'ngTouch',
  'ui.utils',
  'ngStorage',
  'LocalStorageModule'
]);

