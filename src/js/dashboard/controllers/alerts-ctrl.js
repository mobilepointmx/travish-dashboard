/**
 * Alerts Controller
 */
(function(){
  'use strict'
  var app = angular.module('Dashboard');
  app.controller('pipeCtrl', ['ResourceFactory','$scope','SchedulesFactory', 'SweetAlert', '$state', '$modal', '$rootScope', function (ResourceFactory, $scope, SchedulesFactory, SweetAlert, $state,$modal, $rootScope) {
    $rootScope.section = 'Dashboard';
    $rootScope.title = "Dashboard";
    var ctrl = $scope;
    $scope.dato = 5;
    ctrl.displayed = [];
    $scope.statistics = {};
    $scope.showDetails = function(obj){
      SweetAlert.swal({title: "HTML <small>Title</small>!",
        text: "<qrcode data='string' download></qrcode>",
        html: true });
    };

    (function(){
      SchedulesFactory.getStatistics()
        .then(function(response){
          console.log(response.data);
          if(response.status === 200) {

            $scope.statistics = response.data;
          }
        });
    })();

    /**
     * abre la modal
     */
    $scope.openModal = function (size, row)
    {
      console.log(row);
      var modalInstance = $modal.open({
        templateUrl: 'myModal.html',
        controller: 'myModalController',
        size: size,
        resolve: {
          Items: function() //scope del modal
          {
            return 'Travish Client';
          },
          row: function() //scope del modal
          {
            return row;
          }
        }
      });
    }

    $scope.openModalStatus = function (size, row)
    {
      console.log(row);
      var modalInstance = $modal.open({
        templateUrl: 'changeStatus.html',
        controller: 'StatusChangeController',
        size: size,
        resolve: {
          Items: function() //scope del modal
          {
            return 'Travish Client';
          },
          row: function() //scope del modal
          {
            return row;
          }
        }
      });
    }

    $scope.statusChange = function(request){
      var status = "";
      if(request.status === "completed") status = "storage";
      if(request.status === "storage") status = "out_delivered";
      if(request.status === "out_delivered") status = "delivered";

      if(request.status === "completed" || request.status === "storage" || request.status === "out_delivered"){





        SweetAlert.swal({
            title: "Change Status "+ request.status +" to "+ status +". \nAre you sure?",
            text: "You will not be able to return this status again!! " + request.request_id.toString(),
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",confirmButtonText: "Yes, change it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            closeOnCancel: true },
          function(isConfirm){

            if (isConfirm) {

              SchedulesFactory.updateStatus(request.request_id, request.status)
                .then(function(result){
                  if(result.status===200){
                    SweetAlert.swal("Changed!", "The status "+request.status+" has been changed to " + status + ".", "success");

                  }else{
                    SweetAlert.swal("Error!", "The status has not been changed.", "error");
                  }
                  $state.go($state.current, {}, {reload: true});
                },
                function(err){
                  SweetAlert.swal("Error!", "The status has not been changed.", "error");

                });

            } else {
             // SweetAlert.swal("Cancelled", "The status has not been changed.", "error");
            }
          });
      }else{
        SweetAlert.swal("Error!", "The status has not been changed.", "error");
      }


    }

    ctrl.callServer = function callServer(tableState) {
      ctrl.isLoading = true;
      var pagination = tableState.pagination;
      console.log("en callserver");
      console.log(tableState.search.predicateObject)
      var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
      var number = pagination.number || 20;  // Number of entries showed per page.

      SchedulesFactory.getSchedules(start, number, tableState.search.predicateObject).then(function(resp){

        ResourceFactory.getPage(start, number, tableState).then(function (result) {
          $scope.datetime = new Date();
          ctrl.displayed = resp.data;
          tableState.pagination.numberOfPages = Math.ceil(resp.numberOfPages / number);//set the number of pages so the pagination can update
          ctrl.isLoading = false;
        });
      });

    };

  }]);

  app.controller('myModalController', ['$scope','$modalInstance','Items', 'row', function($scope, $modalInstance,Items, row)
  {
    $scope.items = Items;
    $scope.row = row;
    $scope.qr = row.User.CreditCard.cardholderName + " luggage " + row.luggage_amount + " Service-id " + row.status;
    console.log(row);
    $scope.save = function (param)
    {

      console.log(param)
    };

    $scope.cancel = function ()
    {
      $modalInstance.dismiss('cancel');
    };
  }]);


  app.controller('StatusChangeController', ['$scope','$modalInstance','Items', 'row', 'SweetAlert', 'SchedulesFactory','$state', function($scope, $modalInstance,Items, row, SweetAlert, SchedulesFactory, $state)
  {
    $scope.items = Items;
    $scope.row = row;
    $scope.qr = row.User.username + " luggage " + row.luggage_amount + " Service-id " + row.Request.request_id;
    console.log(row);
    $scope.change = "";
    $scope.message = "";
    $scope.band_alert =false;
    $scope.band_success = false;
    $scope.save = function (row, status)
    {
      $scope.band_alert =false;
      $scope.band_success = false;
      $scope.change = status;
      SchedulesFactory.updateStatus(row.Request.request_id, status, row.id)
      .then(function(result){

        if(result.status===200){

          $scope.message = "The status " + row.Request.status + " has been changed to " + status + ".";
          $scope.band_alert =false;
          $scope.band_success = true;

        }else{
          $scope.message = result.message;
          $scope.band_alert =true;
          $scope.band_success = false;
        }
        $state.go($state.current, {}, {reload: true});
      },
      function(err){
        $scope.message = err;

      });

    };

    $scope.cancel = function ()
    {
      $modalInstance.dismiss('cancel');
    };
  }]);

})();

