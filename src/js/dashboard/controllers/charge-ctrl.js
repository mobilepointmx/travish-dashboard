/**
 * Created by IvanIsrael on 06/10/2015.
 */
(function(){
  var app= angular.module('Dashboard');
  app.controller('ChargeCtrl', ['$scope', 'ChargeFactory','$rootScope', 'SweetAlert', function ($scope, ChargeFactory, $rootScope, SweetAlert) {
    $rootScope.section = "Settings / Charge";
    $rootScope.title = "Charge";
    $scope.feeData = {
      minimum_fee: 0,
      minimum_charge: 0.0,
      extra_bag_fee: 1,
      extra_charge: 0.0
    };

    $scope.arrow = false;

    (function(){
      ChargeFactory.getFee()
        .then(function(response){
          if(response !== null){
            $scope.feeData.minimum_fee=response.minimum_fee;
            $scope.feeData.minimum_charge= response.minimum_charge;
            $scope.feeData.extra_bag_fee= response.extra_bag_fee;
            $scope.feeData.extra_charge= response.extra_charge;
          }

        },
        function(err){
          SweetAlert.swal("Error!","Something is wrong.", "error");
        });

    })();

    $scope.submit = function(){
      $scope.arrow = false;
      $scope.authError = "";
      if($scope.feeData.minimum_fee >0 && $scope.feeData.minimum_charge>0 && $scope.feeData.extra_bag_fee>0 &&  $scope.feeData.extra_charge>0){
          ChargeFactory.updateFee($scope.feeData).then(function(response){
            console.log("EN error " + response.data);
            if(response !== null){
              if(response.data !== null && !response.data.error){
                SweetAlert.swal("Updated!", "Service Fee has been updated", "success");
              }else if(response.data !== null && response.data.error){

                for(var e in response.data.error){
                  console.log(e);
                  $scope.authError += response.data.error[e].message + "\n";
                }
                SweetAlert.swal("Error!", $scope.authError, "error");
              }else{
                SweetAlert.swal("Error!","Something is wrong.", "error");
              }
            }else{
              SweetAlert.swal("Error!","Something is wrong.", "error");
            }


          }, function(err){
            SweetAlert.swal("Error!","Something is wrong.", "error");
          });

      }else{
        SweetAlert.swal("Error!", "Fee can't be 0.", "error");
        //$scope.authError = "Fee can't be 0.";
      }
    };

  }]);
})();
