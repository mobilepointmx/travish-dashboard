/**
 * Created by IvanIsrael on 29/10/2015.
 */
(function(){
  var app= angular.module('Dashboard');
  app.controller('DepotCtrl', ['$scope', 'DepotFactory','$rootScope', 'SweetAlert', function ($scope, DepotFactory, $rootScope, SweetAlert) {
    $rootScope.section = "Settings / Address";
    $rootScope.title = "Address";
    $scope.map = { center: { latitude: 45, longitude: -73 }, zoom: 8 };
    $scope.warehouse = {
      name: "",
      coordinates: "",
      address: ""
    };

    (function(){
      DepotFactory.getDepot()
        .then(function(response){
          if(response !== null){
            $scope.warehouse.name=response.name;
            $scope.warehouse.coordinates= response.latitude + ","+response.longitude;
            $scope.warehouse.address= response.address;
          }

        },
        function(err){
          SweetAlert.swal("Error!","Something is wrong.", "error");
        });

    })();

    $scope.submit = function(){
      var coordinates = $scope.warehouse.coordinates.split(",");
      console.log(coordinates.length);

      var regex = /^(-?\d{1,2}\.\d{6,8}),(-?\d{1,2}\.\d{6,8})$/;
      if(coordinates.length == 2){
        $scope.warehouse.latitude = coordinates[0];
        $scope.warehouse.longitude = coordinates[1];
        var valid_coord = coordinates[0].trim()+","+coordinates[1].trim();
        coordinates[0] = coordinates[0].trim();
        coordinates[1] = coordinates[1].trim();
        if(regex.test(valid_coord)){
          $scope.warehouse.coordinates = valid_coord;
          console.log($scope.warehouse);
          DepotFactory.updateDepot($scope.warehouse)
            .then(function(resp){
              SweetAlert.swal("Success!","Address have been save.", "success");
            }, function(err){
              SweetAlert.swal("Error!","Something is wrong. Try again.", "error");
            });

        }else{
          SweetAlert.swal("Error!","The coordinates are not correct.", "error");
        }
      }else{
        SweetAlert.swal("Error!","The coordinates are not correct.", "error");
      }


    };

  }]);
})();
