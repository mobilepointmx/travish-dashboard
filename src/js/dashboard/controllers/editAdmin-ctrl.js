/**
 * Created by IvanIsrael on 07/10/2015.
 */
(function(){
  'use strict'
  var app = angular.module('Dashboard');
  app.controller('pipeAdminCtrl',['$scope', '$state','AdminFactory','$rootScope', function ($scope, $state, AdminFactory, $rootScope) {
    $scope.displayed = [];
    $rootScope.section = " Settings / Users";
    $rootScope.title = "Users";
    $scope.callServer = function callServer(tableState) {
      $scope.isLoading = true;
      var pagination = tableState.pagination;
      //console.log(tableState.search.predicateObject.Client)
      var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
      var number = pagination.number || 20;  // Number of entries showed per page.
      var search = tableState.search.predicateObject|| "";
      console.log(search);
      AdminFactory.getAllAdmins(start, number, search).then(function(resp){

        $scope.displayed = resp;
        $scope.isLoading = false;
      });

    };

  }]);
})();


(function(){
  'use strict'
  var app = angular.module('Dashboard');
  app.controller('AdminCtrl',['$scope', '$state','AdminFactory', '$stateParams', 'SweetAlert', '$rootScope', function ($scope, $state, AdminFactory, $stateParams, SweetAlert,$rootScope) {
    $rootScope.section = "Settings / Users / Edit";
    $rootScope.title = "Edit";
    $scope.registerData = {
      email: "",
      password: "12345678",
      confirm: "",
      phone: "",
      name: ""
    };

    $scope.id = $stateParams.id;
    if($scope.id){
      AdminFactory.getAdmin($scope.id)
        .then(function(resp){
          $scope.registerData.email = resp.data.username || "";
          $scope.registerData.phone = resp.data.phone || "";
          $scope.registerData.name = resp.data.AdminProfile!==null?resp.data.AdminProfile.name:"";
          $scope.registerData.password = resp.data.password || "12345678";
        })
        .catch(function(err){

        });
    }

    $scope.submit = function(){
      var model ={};
      var band = true;
      $scope.arrow = false;
      $scope.authError = "";
      $scope.user_class = "warning-user";
      model.phone = $scope.registerData.phone;
      model.name = $scope.registerData.name;
      model.username = $scope.registerData.email;
      var password =$scope.registerData.password;
      //if superadmin want to update admin password
      if(password.trim() !== "" && password!== "12345678"){
        if(($scope.registerData.password === $scope.registerData.confirm)){
          if($scope.registerData.password.length >= 8){
            model.password = $scope.registerData.password;
            model.confirm = $scope.registerData.confirm;
          }else{
            //$scope.arrow = false;
            //$scope.user_class = "warning-user";
            //$scope.authError = "Password must be longer than 8";
            SweetAlert.swal("Error!", "Password must be longer than 8.", "error");
            band = false;
          }

        }else{
          SweetAlert.swal("Error!", "Password and Confirm Password must be same.", "error");
          //$scope.arrow = false;
          //$scope.user_class = "warning-user";
          //$scope.authError = "Password and Confirm Password must be same.";
          band = false;
        }
      }

      if(band){

        AdminFactory.updateAdmin(model, $scope.id).then(function(response){
          if(!response.data.error){
            $scope.registerData.email = response.data.username || "";
            $scope.registerData.phone = response.data.phone || "";
            $scope.registerData.name = response.data.AdminProfile!==null?response.data.AdminProfile.name:"";
            $scope.registerData.password = response.data.password || "12345678";
            //$scope.arrow = true;
            //$scope.user_class = "valid-user";
            //$scope.authError = "User Updated";
            SweetAlert.swal("Updated!", "User has been updated", "success");
          }else{
            for(var e in response.data.error){
              console.log(e);
              $scope.authError += response.data.error[e].message + "\n";
            }
            SweetAlert.swal("Error!", $scope.authError, "error");
          }

        });
      }

    };

  }]);
})();
