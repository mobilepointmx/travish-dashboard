/**
 * Master Controller
 */
(function(){
  'use strict'
  angular.module('Dashboard')
    .controller('MasterCtrl', ['$rootScope','$scope', '$cookieStore', 'authService', '$state', MasterCtrl]);

  function MasterCtrl($rootScope, $scope, $cookieStore, authService, $state) {
    /**
     * Sidebar Toggle & Cookie Control
     *
     */

    var mobileView = 1201;
    $rootScope.viewMenu = true;
    $rootScope.section = "Dashboard";
    $rootScope.title = "Dashboard";

    $scope.getWidth = function() { return window.innerWidth; };

    $scope.$watch($scope.getWidth, function(newValue, oldValue)
    {
      if(newValue >= mobileView)
      {
        if(angular.isDefined($cookieStore.get('toggle')))
        {
          if($cookieStore.get('toggle') == false)
          {
            $rootScope.toggle = false;
          }
          else
          {
            $rootScope.toggle = true;
          }
        }
        else
        {
          $rootScope.toggle = true;
        }
      }
      else
      {
        $rootScope.toggle = false;
      }

    });


    $scope.toggleSidebar = function()
    {

      $rootScope.toggle = ! $scope.toggle;

      $cookieStore.put('toggle', $scope.toggle);
    };

    $scope.logout = function(){
      authService.logOut();
      $state.go('login');
    };


    window.onresize = function() { $scope.$apply(); };
  }

})();
