/**
 * Created by IvanIsrael on 25/09/2015.
 */
(function(){
  var app= angular.module('Dashboard');
  app.controller('ReportsCtrl', ['$scope', 'ReportsFactory', '$rootScope', function ($scope, ReportsFactory, $rootScope) {
    $scope.isLoading = true;
    $scope.table_height = "table-heigh-responsive";
    $scope.message = "Choose a date range ...";
    $scope.export_action = 'pdf';
    $rootScope.section = 'Reports';
    $rootScope.title = "Reports";
    //In controller
    $scope.exportAction = function(file){
      $scope.export_action = file;
      switch($scope.export_action){
        case 'pdf': $scope.$broadcast('export-pdf', {});
          break;
        case 'excel': $scope.$broadcast('export-excel', {});
          break;
        case 'doc': $scope.$broadcast('export-doc', {});
          break;
        default: console.log('no event caught');
      }
    }

    $scope.reports =  function reports(tableState) {
     //ctrl.isLoading = true;
      var pagination = tableState.pagination;
      var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
      var number = pagination.number || 20;  // Number of entries showed per page.
      if(typeof tableState.search.predicateObject !== 'undefined' && typeof tableState.search.predicateObject.range !== 'undefined'){
        console.log(tableState.search.predicateObject.range);
        ReportsFactory.getReports(0, 0, '', tableState.search.predicateObject.range)
          .then(function(response){

            if(response.data.length > 0){
              if(response.data.length >0 && response.data.length <= 6){
                $scope.table_height = "table-heigh-responsive";
              }
              $scope.displayed = response.data;
              $scope.isLoading = false;
              $scope.table_height="";
            }else{
              $scope.table_height = "table-heigh-responsive";
              $scope.isLoading = true;
            }

          });
      }

    };

  }]);
})();
