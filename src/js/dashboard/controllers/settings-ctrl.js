/**
 * Created by IvanIsrael on 29/10/2015.
 */
(function(){
  var app= angular.module('Dashboard');
  app.controller('SettingsCtrl', ['$scope', 'ReportsFactory', '$rootScope', function ($scope, ReportsFactory, $rootScope) {
    $rootScope.section = "Settings";
    $rootScope.title = "Settings";

  }]);
})();
