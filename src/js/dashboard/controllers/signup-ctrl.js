/**
 * Created by IvanIsrael on 06/10/2015.
 */
(function(){
  var app= angular.module('Dashboard');
  app.controller('SignUpCtrl', ['$scope', 'SignUpFactory', '$stateParams', 'SweetAlert', '$rootScope', function ($scope, SignUpFactory, $stateParams, SweetAlert, $rootScope) {
    $rootScope.section = " Settings / Users / Register";
    $rootScope.title = "Register";
    console.log($stateParams.portfolioId);
    $scope.registerData = {
      email: "",
      password: "",
      confirm: "",
      phone: "",
      name: ""
    };
    //$scope.user_class = "warning-user";
    //$scope.arrow = false;


    $scope.submit = function(){

      $scope.arrow = false;
      $scope.authError = "";
      if($scope.registerData.password === $scope.registerData.confirm ){
        if($scope.registerData.password.length >= 8){
          SignUpFactory.createAdmin($scope.registerData).then(function(response){
            if(!response.data.error){
              $scope.registerData = {
                email: "",
                password: "",
                confirm: "",
                phone: "",
                name:""
              };
              //$scope.user_class = "valid-user";
              //$scope.arrow = true;
              //$scope.authError = "User Created";
              SweetAlert.swal("Created!", "User has been created", "success");
            }else{
              for(var e in response.data.error){
                console.log(e);
                $scope.authError += response.data.error[e].path +": "+ response.data.error[e].message + "\n";
              }
              SweetAlert.swal("Error!", $scope.authError, "error");
            }

          });
        }
        else{
          SweetAlert.swal("Error!", "Password must be longer than 8.", "error");
          //$scope.user_class = "warning-user";
          //$scope.arrow = false;
          //$scope.authError = "Password must be longer than 8";
        }
      }else{
        SweetAlert.swal("Error!", "Password and Confirm Password must be same.", "error");
        //$scope.user_class = "warning-user";
        //$scope.arrow = false;
        //$scope.authError = "Password and Confirm Password must be same.";
      }
    };

  }]);
})();
