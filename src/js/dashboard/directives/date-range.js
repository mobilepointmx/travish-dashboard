/**
 * Created by IvanIsrael on 25/09/2015.
 */
(function(){
  angular.module('Dashboard').directive('stDateRange', ['$timeout', function ($timeout) {
    return {
      restrict: 'E',
      require: '^stTable',
      scope: {
        from: '=',
        to: '='
      },
      templateUrl: 'stDateRange.html',

      link: function (scope, element, attr, table) {

        var inputs = element.find('input');
        var inputBefore = angular.element(inputs[0]);
        var inputAfter = angular.element(inputs[1]);
        var predicateName = attr.predicate;

        [inputBefore, inputAfter].forEach(function (input) {

          input.bind('blur', function () {


            var query = {};

            if (!scope.isBeforeOpen && !scope.isAfterOpen) {

              if (scope.from) {
                console.log("evaluar " + scope.from.constructor.toString().indexOf("Date"));
                query.from = scope.from;
              }else{
                scope.from = null;
              }

              if (scope.to) {

                if(scope.from.getTime() < scope.to.getTime()){
                  query.to = scope.to;
                  console.log(scope.to.getTime());
                }
                else
                  scope.to = null;
              }else{
                scope.to = null;
              }
              //console.log(query);
              scope.$apply(function () {
                table.search(query, predicateName);
              })
            }
          });
        });

        function open(before) {
          return function ($event) {
            $event.preventDefault();
            $event.stopPropagation();

            if (before) {
              scope.isBeforeOpen = true;
            } else {
              scope.isAfterOpen = true;
            }
          }
        }

        scope.openBefore = open(true);
        scope.openAfter = open();
      }
    }
  }]);
})();
