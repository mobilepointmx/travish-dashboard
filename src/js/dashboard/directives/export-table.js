/**
 * Created by IvanIsrael on 25/09/2015.
 */
(function(){
//export html table to pdf, excel and doc format directive
  var exportTable = function($window){
    var link = function(scope, element, attr, table){
          scope.$on('export-pdf', function(e, d){
            //console.log(element);
            //var myEl = angular.element(document.querySelector('.delete-table'));
            //var e = myEl.clone();
            //myEl.remove();
            //element.tableExport({type:'pdf', escape:false, pdfLeftMargin:10, pdfFontSize:10});
            //myEl.empty();
            //myEl = e;
            //console.log(element);
            //element.tableExport({type:'pdf', escape:'false', htmlContent:'false', pdfFontSize:8,separator: '-',pdfLeftMargin:1});

          //  var doc = new jsPDF();
          //  var x = angular.element(document.querySelector('#export-report'));
          //  var y = x.find('.delete-table');
          //  y.remove();
          //  // We'll make our own renderer to skip this editor
          //  var specialElementHandlers = {
          //    '.delete-table': function(element, renderer){
          //      return true;
          //    }
          //  };
          //
          //  //x.each(function( index ) {
          //  //  console.log( index + ": " + $( this ).text() );
          //  //});
          //
          //  console.log(x.find('table').get(0));
          //// All units are in the set measurement for the document
          //// This can be changed to "pt" (points), "mm" (Default), "cm", "in"
          //doc.fromHTML(x.find('table').get(0), 15, 15, {
          //    'width': 200,
          //
          //  });
          //  doc.save('Test.pdf');
            /*
            * We get titles and information from table
            *
            */
            var columns = [];
            var exportReport = angular.element(document.querySelector('#export-report'));
            var exportHeaders = exportReport.find('thead').find('#export-headers').find('th');

            exportHeaders.each(function( index ) {
              columns.push($(this).text());
             //console.log( index + ": " + $( this ).text() );
            });

            var exportBody = exportReport.find('tbody').find('.export-body');
            console.log(exportBody.size());
            var rows = [];
            var inside_rows = [];
            exportBody.each(function( index, elmnt) {
              //get report information
              var td = $(elmnt).find('td');
              td.each(function(index2){
                inside_rows.push($(this).text())

              });
              rows.push(inside_rows);
              inside_rows = [];
              //console.log( index + ": " + $( this ).text() );
            });

            var doc = new jsPDF('p', 'pt');
            doc.autoTable(columns, rows, {
              margin: {top: 20, left:20},
              beforePageContent: function(data) {
                doc.text("Header", 40, 30);
              }
            });

            //Generate pdf
            doc.save('table.pdf');

          });

          scope.$on('export-excel', function(e, d){

            var ejemplo = element.clone(true, true);
            if(ejemplo === element) console.log("Son iguales");
            var myEl = element.find('.delete-table');
            myEl.remove();
            console.log(ejemplo);
            console.log(e);
            console.log(d);
            console.log(table);
            element.tableExport({type:'excel', escape:false});
            $window.location.reload();

            //
            //var inputs = element.find('input');

          //  element.table2excel({
          //    exclude: ".delete-table",
          //    name: "Worksheet Name",
          //    filename: "SomeFile" //do not include extension
          //  });
          //
          });
          scope.$on('export-doc', function(e, d){
            var myEl = angular.element(document.querySelector('.delete-table'));
            myEl.remove();
            element.tableExport({type: 'doc', escape:false});
          });
    }
    return {
      restrict: 'C',
      link: link
    }
  }
  angular.module('Dashboard').directive('exportTable', ['$window', exportTable]);
})();
