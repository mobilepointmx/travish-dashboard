/**
 * Created by IvanIsrael on 07/10/2015.
 */
(function(){
  'use strict';
  var app= angular.module('Dashboard');
  app.factory('AdminFactory', ['$q', '$filter', '$timeout', 'ngAuthSettings', '$http', function ($q, $filter, $timeout, ngAuthSettings, $http) {
    var adminFactory = {};
    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var _getAllAdmins = function(start, number, search){
      var name = search.Client || "";
      var email = search.Email || "";
      var deferred=$q.defer();
      var route = serviceBase + "admin";
      console.log("email " + email);
      route += name!==null && email !== null? "/?name="+name+"&email="+email:"/";

      $http.get(route).success(function(response, status, headers, config){
        deferred.resolve(response);
      }).error(function(err, status, headers, config){
        deferred.reject(err);
      });
      return deferred.promise;
    };

    var _getAdmin = function(user){

      var deferred=$q.defer();
      var route = serviceBase + "admin/" + user;

      $http.get(route).success(function(response, status, headers, config){
        var model = {};
        console.log(response);
        model.data = response;
        deferred.resolve(model);
      }).error(function(err, status, headers, config){
        deferred.reject(err);
      });
      return deferred.promise;

    };

    var _updateAdmin = function(admin, user){
      var data = admin;
      var deferred=$q.defer();
      var route = serviceBase + "admin/"+ user;

      $http.put(route, data, { headers: { 'Content-Type': 'application/json' }
      }).success(function(response, status, headers, config){
        var model = {};
        console.log("update admin ");
        console.log(response);
        model.data = response;
        deferred.resolve(model);
      }).error(function(err, status, headers, config){
        deferred.reject(err);
      });
      return deferred.promise;

    };

    adminFactory.getAdmin = _getAdmin;
    adminFactory.getAllAdmins = _getAllAdmins;
    adminFactory.updateAdmin = _updateAdmin;

    return adminFactory;

  }]);
})();
