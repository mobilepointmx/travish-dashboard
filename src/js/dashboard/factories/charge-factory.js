/**
 * Created by IvanIsrael on 06/10/2015.
 */
(function(){
  'use strict';
  var app= angular.module('Dashboard');
  app.factory('ChargeFactory', ['$q', '$filter', '$timeout', 'ngAuthSettings', '$http', function ($q, $filter, $timeout, ngAuthSettings, $http) {
    var chargeFactory = {};
    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var _getFee = function(fee){

      var deferred=$q.defer();
      var route = serviceBase + "fee";

      $http.get(route).success(function(response, status, headers, config){
        deferred.resolve(response);
      }).error(function(err, status, headers, config){
        deferred.reject(err);
      });
      return deferred.promise;
    };

    var _updateFee = function(fee){
      var data = {
        minimum_fee: fee.minimum_fee,
        minimum_charge: fee.minimum_charge,
        extra_bag_fee: fee.extra_bag_fee,
        extra_charge: fee.extra_charge
      };

      var deferred=$q.defer();
      var route = serviceBase + "fee";

      $http.put(route, data, { headers: { 'Content-Type': 'application/json' }
      }).success(function(response, status, headers, config){
        var model = {};
        console.log("create admin ");
        console.log(response);
        model.data = response;
        deferred.resolve(model);
      }).error(function(err, status, headers, config){
        deferred.reject(err);
      });
      return deferred.promise;

    };

    chargeFactory.getFee = _getFee;
    chargeFactory.updateFee = _updateFee;

    return chargeFactory;

  }]);
})();
