/**
 * Created by IvanIsrael on 29/10/2015.
 */
(function(){
  'use strict';
  var app= angular.module('Dashboard');
  app.factory('DepotFactory', ['$q', '$filter', '$timeout', 'ngAuthSettings', '$http', function ($q, $filter, $timeout, ngAuthSettings, $http) {
    var chargeFactory = {};
    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var _getDepot = function(address){

      var deferred=$q.defer();
      var route = serviceBase + "dashboard/address";

      $http.get(route).success(function(response, status, headers, config){
        deferred.resolve(response);
      }).error(function(err, status, headers, config){
        deferred.reject(err);
      });
      return deferred.promise;
    };

    var _updateDepot = function(data){
      var model = {};
      model.address = {
        name: data.name,
        latitude: data.latitude,
        longitude: data.longitude,
        address: data.address
      };

      var deferred=$q.defer();
      var route = serviceBase + "dashboard/address";

      $http.put(route, model, { headers: { 'Content-Type': 'application/json' }
      }).success(function(response, status, headers, config){
        var model = {};
        console.log(response);
        model.data = response;
        deferred.resolve(model);
      }).error(function(err, status, headers, config){
        deferred.reject(err);
      });
      return deferred.promise;

    };

    chargeFactory.getDepot = _getDepot;
    chargeFactory.updateDepot = _updateDepot;

    return chargeFactory;

  }]);
})();
