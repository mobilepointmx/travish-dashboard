/**
 * Created by IvanIsrael on 21/09/2015.
 */
(function(){
  'use strict';

  angular.module('Dashboard').factory('LoginFactory', ['$resource', 'ngAuthSettings', function ($resource, ngAuthSettings) {

    var url = ngAuthSettings.apiServiceBaseUri;

    return $resource(url, {}, {
      active: {url: url + "/company/activation/",method: 'POST', isArray: false}
    });

  }]);
})();

