/**
 * Created by IvanIsrael on 25/09/2015.
 */
(function(){
  'use strict';

  angular.module('Dashboard').factory('ReportsFactory', ['$resource', 'ngAuthSettings','$rootScope', '$log', '$http', '$q', function ($resource, ngAuthSettings, $rootScope, $log, $http,$q) {

    var reportsFactory = {};
    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var getReports = function(start, limit,status, range){
      var range = range || {};
      var status = status || '';
      var deferred=$q.defer();
      var route = serviceBase + "dashboard/reports?start=" +start+"&limit="+limit;
      route += range.from? "&from=" + range.from: "&from= ";
      route += range.to? "&to="+range.to:"&to= ";

      $http.get(route).success(function(response, status, headers, config){
        var model = {};
        model.data = response.reports_range;
        model.numberOfPages = response.count || 0;
        deferred.resolve(model);
      }).error(function(err, status, headers, config){
        deferred.reject(err);
      });
      return deferred.promise;

    };
    reportsFactory.getReports = getReports;

    return reportsFactory;

  }]);
})();
