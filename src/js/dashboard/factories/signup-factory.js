/**
 * Created by IvanIsrael on 06/10/2015.
 */
(function(){
  'use strict';
  var app= angular.module('Dashboard');
  app.factory('SignUpFactory', ['$q', '$filter', '$timeout', 'ngAuthSettings', '$http', function ($q, $filter, $timeout, ngAuthSettings, $http) {
    var signUpFactory = {};
    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var _createAdmin = function(user){
      var data = {
        username: user.email,
        password: user.password,
        phone: user.phone,
        name: user.name
      };

      var deferred=$q.defer();
      var route = serviceBase + "admin/signup";

      $http.post(route, data, { headers: { 'Content-Type': 'application/json' }
      }).success(function(response, status, headers, config){
        var model = {};
        console.log("create admin ");
        console.log(response);
        model.data = response;
        deferred.resolve(model);
      }).error(function(err, status, headers, config){
        deferred.reject(err);
      });
      return deferred.promise;

    };


    signUpFactory.createAdmin = _createAdmin;
    //signUpFactory.getAdmin = _getAdmin;

    return signUpFactory;

  }]);
})();
