/**
 * Created by IvanIsrael on 22/09/2015.
 */
(function(){
  var app= angular.module('Dashboard');
app.factory('ResourceFactory', ['$q', '$filter', '$timeout', function ($q, $filter, $timeout) {

  //this would be the service to call your server, a standard bridge between your model an $http

  // the database (normally on your server)
  var randomsItems = [];

  //fake call to the server, normally this service would serialize table state to send it to the server (with query parameters for example) and parse the response
  //in our case, it actually performs the logic which would happened in the server
  function getPage(start, number, params) {

    var deferred = $q.defer();

    var filtered = params.search.predicateObject ? $filter('filter')(randomsItems, params.search.predicateObject) : randomsItems;

    if (params.sort.predicate) {
      filtered = $filter('orderBy')(filtered, params.sort.predicate, params.sort.reverse);
    }

    var result = filtered.slice(start, start + number);

    $timeout(function () {
      //note, the server passes the information about the data set size
      deferred.resolve({
        data: result,
        numberOfPages: Math.ceil(filtered.length / number)
      });
    }, 1500);


    return deferred.promise;
  }

  return {
    getPage: getPage
  };

}]);
})();

(function () {

  'use strict';

  var app = angular.module("Dashboard");
  app.factory('SchedulesFactory', ['$resource', '$cookies', 'ngAuthSettings', '$rootScope', '$log', '$http', '$q',function ($resource, $cookies, ngAuthSettings, $rootScope, $log, $http, $q) {
    var callsFactory = {};
    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var getSchedules = function(start, limit, search){
      var search = search || {};
      var deferred = $q.defer();
      var status = search.status || 'completed';
      console.log("status " + status);
      var route = serviceBase + "dashboard/?start=" +start+"&limit="+limit;
      if(search.Client) route = route + "&username=" + search.Client;
      if(search.service) route = route + "&service=" + search.service;
      if(search.amount) route = route + "&amount=" + search.amount;
      if(status) route = route + "&status=" + status;
      if(search.terminal) route + "&terminal=" + search.terminal;

      $http.get(route).success(function(response, status, headers, config){
        var model = {};
        model.data = response.data;
        model.numberOfPages = response.count || 0;
        deferred.resolve(model);
      }).error(function(err, status, headers, config){
        deferred.reject(err);
      });
      return deferred.promise;

    };

    var updateStatus = function(request_id, status, schedule_id){
      console.log(request_id + " " + status + " " + schedule_id)
      var deferred=$q.defer();
      var data = {
        status: status,
        schedule_id: schedule_id
      };

      var route = serviceBase + "dashboard/request/"+request_id+"/status/";
      $http.put(route, data, { headers: { 'Content-Type': 'application/json' }
      }).success(function (response, status) {
        console.log("status " + status);
        response.status = status;
        deferred.resolve(response);
      }).error(function (err) {
        deferred.reject(err);
      });
      return deferred.promise;
    }

    var getStatistics = function(){
      var deferred=$q.defer();
      var route = serviceBase + "dashboard/statistics";
      $http.get(route).success(function(response, status, headers, config){
        var model = {};
        model.data = response;
        model.status = status;
        deferred.resolve(model);
      }).error(function(err, status, headers, config){
        deferred.reject(err);
      });
      return deferred.promise;
    };



    callsFactory.getSchedules = getSchedules;
    callsFactory.updateStatus = updateStatus;
    callsFactory.getStatistics = getStatistics;
    return callsFactory;
  }]);

})();
