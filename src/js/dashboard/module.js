(function(){
  'use strict';
  var app = angular.module('Dashboard', [
    'ui.bootstrap',
    'ui.router',
    'ngCookies',
    'ngStorage',
    'ngResource',
    'LocalStorageModule',
    'smart-table',
    'oitozero.ngSweetAlert',
    'monospaced.qrcode',
    'uiGmapgoogle-maps',

  ]);

  app.config(['$controllerProvider', '$compileProvider', '$filterProvider', '$provide', 'uiGmapGoogleMapApiProvider', function($controllerProvider, $compileProvider, $filterProvider, $provide, uiGmapGoogleMapApiProvider) {

    uiGmapGoogleMapApiProvider.configure({
      //    key: 'your api key',
      v: '3.20', //defaults to latest 3.X anyhow
      libraries: 'weather,geometry,visualization'
    });
    // lazy controller, directive and service
    app.controller = $controllerProvider.register;
    app.directive  = $compileProvider.directive;
    app.filter     = $filterProvider.register;
    app.factory    = $provide.factory;
    app.service    = $provide.service;
    app.constant   = $provide.constant;
    app.value      = $provide.value;
  }])
    .constant('ngAuthSettings', {

      //apiServiceBaseUri: 'https://localhost:8000/api/',
      //clientId: 'mobileV1',
      //clientSecret: 'abc123456'
      apiServiceBaseUri: 'https://ec2-52-88-174-216.us-west-2.compute.amazonaws.com:8000/api/',
      clientId: 'WebAdmin',
      clientSecret: 'TravishAdmin'
    })
    .run(['authService','$state', '$rootScope', '$injector', function (authService,$state, $rootScope, $injector) {

      $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
        var isAuth = authService.fillAuthData();
        if(typeof toState.data !== 'undefined')
        {
          var stateName = '';
          if (typeof toState.data.isNotRequireLogin === 'undefined' && isAuth === false) {
            stateName = 'login';
          }
          else if(toState.data.isNotRequireLogin === true && isAuth === true && toState.name !== 'access.signout'){
            stateName = 'index.dash';
          }

          if (stateName != ''){
            event.preventDefault();
            $state.go(stateName);
          }
        }
        else if(isAuth === false){
          var $stateinjector = $injector.get("$state");
          $stateinjector.go("login");
        }
      });
    }]);

})();

