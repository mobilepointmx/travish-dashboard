/**
 * Route configuration for the Dashboard module.
 */
(function(){
  angular.module('Dashboard').config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {

      // For unmatched routes
      $urlRouterProvider.otherwise(function ($injector) {
        var $state = $injector.get("$state");
        $state.go("login");
      });

      // Application routes
      $stateProvider
        .state('index', {
          url: '/index',
          templateUrl: 'main.html',

        })
        .state('index.dash', {
          url: '/dash',
          views: {
            'dashboard': {
              templateUrl: 'dashboard.html',
              data:{
                isNotRequireLogin: false
              }
            },
            'sideBar': {
              templateUrl: 'side-bar.html',
            },
            'menuHeader': {
              templateUrl: 'menu-header.html',
            }
          },

        })
        .state('index.reports', {
          url: '/reports',
          views: {
            'dashboard': {
              templateUrl: 'reports.html',
              data:{
                isNotRequireLogin: false
              }
            },
            'sideBar': {
              templateUrl: 'side-bar.html',
            },
            'menuHeader': {
              templateUrl: 'menu-header.html',
            }
          },

        })
        .state('login', {
          url: '/login',
          templateUrl: 'login.html',
          data:{
            isNotRequireLogin: true
          }
        })
        .state('index.edit_admin', {
          url: '/edit_admin/:id',
          views: {
            'dashboard': {
              templateUrl: 'editAdmin.html',
              data:{
                isNotRequireLogin: false
              }
            },
            'sideBar': {
              templateUrl: 'side-bar.html',
            },
            'menuHeader': {
              templateUrl: 'menu-header.html',
            }
          },
        })
        .state('index.register', {
          url: '/register',
          views: {
            'dashboard': {
              templateUrl: 'signup.html',
              data:{
                isNotRequireLogin: false
              }
            },
            'sideBar': {
              templateUrl: 'side-bar.html',
            },
            'menuHeader': {
              templateUrl: 'menu-header.html',
            }
          },
        })
        .state('index.fee', {
          url: '/fee',
          views: {
            'dashboard': {
              templateUrl: 'charge.html',
              data:{
                isNotRequireLogin: false
              }
            },
            'sideBar': {
              templateUrl: 'side-bar.html',
            },
            'menuHeader': {
              templateUrl: 'menu-header.html',
            }
          },
        }).state('index.depot', {
          url: '/depot',
          views: {
            'dashboard': {
              templateUrl: 'depot.html',
              data:{
                isNotRequireLogin: false
              }
            },
            'sideBar': {
              templateUrl: 'side-bar.html',
            },
            'menuHeader': {
              templateUrl: 'menu-header.html',
            }
          },
        })
        .state('index.settings', {
          url: '/settings',
          views: {
            'dashboard': {
              templateUrl: 'settings.html',
              data:{
                isNotRequireLogin: false
              }
            },
            'sideBar': {
              templateUrl: 'side-bar.html',
            },
            'menuHeader': {
              templateUrl: 'menu-header.html',
            }
          },
        })
        .state('index.manage_admin', {
          url: '/manage_admin',
          views: {
            'dashboard': {
              templateUrl: 'adminTables.html',
              data:{
                isNotRequireLogin: false
              }
            },
            'sideBar': {
              templateUrl: 'side-bar.html',
            },
            'menuHeader': {
              templateUrl: 'menu-header.html',
            }
          },
        });
    }]);

})();

