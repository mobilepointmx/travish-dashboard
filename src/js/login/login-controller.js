/**
 * Created by IvanIsrael on 07/09/2015.
 */
/**
 * Master Controller
 */
(function(){
  'use strict';
  var app=angular.module('Dashboard');
app.controller('LoginCtrl', ['$scope', '$cookieStore','authService', '$state', 'localStorageService', function LoginCtrl($scope, $cookieStore, authService, $state, localStorageService) {
    $scope.authError = null;
    $scope.welcome = null;
    $scope.loginData = {
      userName: "",
      password: "",
      useRefreshTokens: true
    };

    var _onActivation = function (data) {
      $scope.welcome = 'Your Account have been activated!';
    };

    var _onActivationError = function (error) {
      $scope.welcome = 'Sorry, we have not activated your account';
    };

    if(typeof ($scope.token)!='undefined' && typeof $scope.flagActive =='undefined' ){

      LoginRepository.active({}, $scope.token, _onActivation, _onActivationError);
      $scope.flagActive = true;
    };

    $scope.login = function () {
      console.log("aqui form " + loginForm.$valid);
      $scope.authError = "";
      if($scope.loginData.userName !== "" && $scope.loginData.password!== ""){

       // authService.logOut();
        authService.login($scope.loginData).then(function (response) {
            console.log(response);
            if(response && !response.error && response.error !== 'invalid_grant'){
              $state.go('index.dash');
            }else{
              console.log("else response ");
              $scope.authError = 'Wrong email or password. Try again.';
            }

          },
          function (err) {
            console.log("err "+err);
            $scope.authError = 'Wrong email or password';
          });
      }else{
        console.log("en else");
        $scope.authError = 'Wrong email or password. Try again.';
      }

    };

  }]);
})();



