/**
 * Created by IvanIsrael on 21/09/2015.
 */
var app = angular.module("Dashboard");
app.config(['$httpProvider', function ($httpProvider) {

  $httpProvider.interceptors.push(['$timeout','$q','$injector','localStorageService',function ($timeout, $q, $injector, localStorageService) {
    var authService, $http, $state, authService;

    // this trick must be done so that we don't receive
    // `Uncaught Error: [$injector:cdep] Circular dependency found`
    $timeout(function () {
      authService = $injector.get('authService');
      $http = $injector.get('$http');
      $state = $injector.get('$state');
    });

    return {

      request: function (config) {

        config.headers = config.headers || {};

        var authData = localStorageService.get('authorizationData');

        if (authData) {
          config.headers.Authorization = 'Bearer ' + authData.token;
        }

        return config;
      },

      responseError: function (rejection) {
        console.log(rejection.status);
        if (rejection.status !== 401) {
          return rejection;
        }

        var deferred = $q.defer();

        var authData = localStorageService.get('authorizationData');

        console.log("authData....");
        console.log(authData);
        if (authData) {
          console.log(authData);
          if (authData.useRefreshTokens) {
            authService.refreshToken();
            deferred.resolve( $http(rejection.config) );
          }
        }
        else{
          console.log("en interceptor saliendo....");
          authService.logOut();
          $state.go('login');
        }

        return deferred.promise;
      }
    };
  }]);

}]);

