/**
 * Created by IvanIsrael on 21/09/2015.
 */
'use strict';
var app = angular.module("Dashboard");
app.factory('authService', ['$http', '$q', 'localStorageService', 'ngAuthSettings', function ($http, $q, localStorageService, ngAuthSettings) {

  var serviceBase = ngAuthSettings.apiServiceBaseUri;
  var authServiceFactory = {};

  var _authentication = {
    isAuth: false,
    userName: "",
    useRefreshTokens: false
  };

  var _externalAuthData = {
    provider: "",
    userName: "",
    externalAccessToken: ""
  };

  var _saveRegistration = function (registration) {


    return $http.post(serviceBase + 'api/account/register', registration).then(function (response) {
      return response;
    });

  };

  var _login = function (loginData) {

    var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;
    data = data + "&client_secret=" + ngAuthSettings.clientSecret;
    data = data + "&client_id=" + ngAuthSettings.clientId;


    var deferred = $q.defer();

    $http.post(serviceBase + 'oauth/token/', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {

      if(response){
        if (loginData.useRefreshTokens) {
          localStorageService.set('authorizationData', { token: response.access_token, userName: loginData.userName, refreshToken: response.refresh_token, useRefreshTokens: true });
        }
        else {
          localStorageService.set('authorizationData', { token: response.access_token, userName: loginData.userName, refreshToken: "", useRefreshTokens: false });
        }
        _authentication.isAuth = true;
        _authentication.userName = loginData.userName;
        _authentication.useRefreshTokens = loginData.useRefreshTokens;
      }

      deferred.resolve(response);
      console.log("En login");
    }).error(function (err, status) {
      _logOut();
      deferred.reject(err);
    });

    return deferred.promise;

  };

  var _logOut = function () {

    localStorageService.remove('authorizationData');

    _authentication.isAuth = false;
    _authentication.userName = "";
    _authentication.useRefreshTokens = false;

  };

  var _fillAuthData = function () {
    var isAuth = false;
    var authData = localStorageService.get('authorizationData');
    if (authData && authData.token) {
      _authentication.isAuth = true;
      _authentication.userName = authData.userName;
      _authentication.useRefreshTokens = authData.useRefreshTokens;
      isAuth = true;
    }
    return isAuth;
  };

  var _refreshToken = function () {
    var deferred = $q.defer();

    var authData = localStorageService.get('authorizationData');

    if (authData) {

      if (authData.useRefreshTokens) {

        var data = "grant_type=refresh_token&refresh_token=" + authData.refreshToken + "&client_id=" + ngAuthSettings.clientId+"&client_secret=" + ngAuthSettings.clientSecret;

        localStorageService.remove('authorizationData');

        $http.post(serviceBase + 'oauth/token/', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {
          console.log("en refresh...");

          localStorageService.set('authorizationData', { token: response.access_token, userName: response.userName, refreshToken: response.refresh_token, useRefreshTokens: true });
          var authData = localStorageService.get('authorizationData');
          console.log(authData);
          deferred.resolve(response);

        }).error(function (err, status) {
          _logOut();
          deferred.reject(err);
        });
      }
    }

    return deferred.promise;
  };

  var _obtainAccessToken = function (externalData) {

    var deferred = $q.defer();

    $http.get(serviceBase + 'api/account/ObtainLocalAccessToken', { params: { provider: externalData.provider, externalAccessToken: externalData.externalAccessToken } }).success(function (response) {

      localStorageService.set('authorizationData', { token: response.access_token, userName: response.userName, refreshToken: "", useRefreshTokens: false });

      _authentication.isAuth = true;
      _authentication.userName = response.userName;
      _authentication.useRefreshTokens = false;

      deferred.resolve(response);

    }).error(function (err, status) {
      _logOut();
      deferred.reject(err);
    });

    return deferred.promise;

  };

  var _registerExternal = function (registerExternalData) {

    var deferred = $q.defer();

    $http.post(serviceBase + 'api/account/registerexternal', registerExternalData).success(function (response) {

      localStorageService.set('authorizationData', { token: response.access_token, userName: response.userName, refreshToken: "", useRefreshTokens: false });

      _authentication.isAuth = true;
      _authentication.userName = response.userName;
      _authentication.useRefreshTokens = false;

      deferred.resolve(response);

    }).error(function (err, status) {
      _logOut();
      deferred.reject(err);
    });

    return deferred.promise;

  };

  authServiceFactory.saveRegistration = _saveRegistration;
  authServiceFactory.login = _login;
  authServiceFactory.logOut = _logOut;
  authServiceFactory.fillAuthData = _fillAuthData;
  authServiceFactory.authentication = _authentication;
  authServiceFactory.refreshToken = _refreshToken;

  authServiceFactory.obtainAccessToken = _obtainAccessToken;
  authServiceFactory.externalAuthData = _externalAuthData;
  authServiceFactory.registerExternal = _registerExternal;

  return authServiceFactory;
}]);
